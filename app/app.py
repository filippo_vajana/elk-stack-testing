import random
import prometheus_client as prom
import os
from time import sleep
import math
import logging
logging.basicConfig(level=logging.DEBUG)

# # GRAPHITE PUSH
# from prometheus_client.bridge.graphite import GraphiteBridge

# gb = GraphiteBridge(('graphite.your.org', 2003))
# # Push once.
# gb.push()
# # Push every 10 seconds in a daemon thread.
# gb.start(10.0)

# LABELS_KEYS = ['deployment', 'stage']
# LABELS_VALUES = ['tickstack', 'testing']

NAMESPACE = 'tickstackdev'

m1 = prom.Counter(name='counter', documentation='', namespace=NAMESPACE)
m2 = prom.Gauge(name='gauge', documentation='', namespace=NAMESPACE)
m2_value = 0
m3 = prom.Histogram(name='histogram', documentation='',
                    buckets=(2, 8, 10), namespace=NAMESPACE)
m4 = prom.Info(name='info', documentation='', namespace=NAMESPACE)
m4_value = 1
m5 = prom.Enum(name='enum', documentation='',
               states=['OK', 'ERROR', 'UPDATING'], namespace=NAMESPACE)


def log_metrics():
    global m2_value
    global m4_value

    # update metrics
    logging.info("Updating metrics")
    m1.inc()

    m2.set(math.sin(math.radians(m2_value)))
    m2_value = (m2_value + 15) % 360

    m3.observe(m2_value)

    rnd = random.randint(0, 10)
    if rnd >= 8:
        m4_value += 1
        m5.state('UPDATING')
    elif rnd <= 2:
        m5.state('ERROR')
        # sleep(2)
    else:
        m5.state('OK')

    m4.info({'version': f'0.0.{m4_value}'})


if __name__ == '__main__':
    # Start up the server to expose the metrics.
    logging.info(
        f"Exposing Prometheus metrics at port: 9090")
    prom.start_http_server(9090)

    # Generate some requests.
    while True:
        log_metrics()
        sleep(0.5)
