# Elasticsearch Stack Evaluation

Objective of this repository is to evaluate the usage of the Elasticsearch Stack for monitoring purposes.
The monitoring activities must primarily focus on collecting metrics about Docker containers and host machine status and resources utilization.

## Evaluation environment

The evaluation environment must mimick a real-world deployment of Orobix application, so we have to consider both in physical machines configuration and software loads.
To meet these criteria we'll use some of our equipment, usually reserved for internal testing, to recreate the **DTR deployment**.

Here a brief summary of the hosts that will be used during the evaluation process:

| hostname        | ip            | role    | type | location |
| --------------- | ------------- | ------- | ---- | -------- |
| swarm-master-01 | 192.168.3.10  | bastion | phy  | lab      |
| swarm-node-01   | 192.168.3.11  | worker  | phy  | lab      |
| swarm-node-02   | 192.168.3.12  | worker  | phy  | lab      |
| obxaigov-vm     | 13.73.148.115 | storage | vm   | azure    |

