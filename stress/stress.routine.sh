#!/bin/bash

while [ 0 -lt 1 ]
do
    sleep 1m
    stress-ng --class cpu --class memory --seq 0 -t 1m
    sleep 1m
done